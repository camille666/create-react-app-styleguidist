import React from 'react';
import MyButton from './components/button/button';
import MyLabel from './components/label/label';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <MyButton size="large" color="deeppink">按钮</MyButton>
      <MyLabel background="deeppink" color="white">这是一个label</MyLabel>
    </div>
  );
}

export default App;
