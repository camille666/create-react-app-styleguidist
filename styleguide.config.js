module.exports = {
    sections: [
      {
        name: '介绍',
        content: 'docs/introduction.md'
      },
      {
        name: '项目文档',
        sections: [
          {
            name: '安装',
            content: 'docs/installation.md',
            description: '介绍安装哪些第三方包'
          },
          {
            name: '配置',
            content: 'docs/configuration.md'
          },
          {
            name: '在线案例',
            external: true,
            href: 'https://react-styleguidist.js.org/examples/basic/'
          }
        ]
      },
      {
        name: 'UI组件',
        content: 'docs/ui.md',
        components: 'src/components/**/*.js',
        exampleMode: 'expand', // 'hide' | 'collapse' | 'expand'
        usageMode: 'expand' // 'hide' | 'collapse' | 'expand'
      }
    ]
}  